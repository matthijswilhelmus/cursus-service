# Cursus service

1. Bouw de service dmv ```mvn clean install```.   
   Hierbij worden meteen de unit- en integratie testen uitgevoerd.  
   De unit- en integratie testen maken gebruik van een H2 memory database.  
   Je hoeft hiervoor dus geen eigen database te draaien.  
   Deliverable is een JAR file te vinden in de target folder.  
2. Start de service dmv ```mvn spring-boot:run```.   
   De main functie is te vinden in de class: src/main/java/nl/kzaconnected/cursus/CursusServiceApplication.  
   De service depend op een mysql database. Draai dus eerst(!) een mysql docker container dmv ```docker-compose up -d cursus_database```.  
   De service draait vervolgens onder localhost:8080.  
3. Service endpoints zijn direct te benaderen of middels een Swagger client, te vinden onder: localhost:8080/swagger-ui.html  

# OWASP Dependency Check

De OWASP dependency check wordt bij het uitvoeren van ```mvn dependency-check:check``` gedraaid.
Dit commando maakt een rapport aan in de Target folder met als naam: dependency-check-report.html

In dit rapport staan kwetsbaarheden die zitten in packages / dependencies e.d. die in dit project zitten.
In de configuratie is van alles mogelijk, bijvoorbeeld de build laten falen wanneer de CVSS score van het rapport hoger is dan een bepaalde waarde
```xml
<failBuildOnCVSS>4</failBuildOnCVSS>
```

# PITest Mutation Testing

Meer informatie over PITest: <http://pitest.org>
Maven Quick Start Guide: <http://pitest.org/quickstart/maven/>

Mogelijkheden:
1. Analyse uitvoeren vanaf Commandline:
  ```mvn org.pitest:pitest-maven:mutationCoverage```
   Let op! De analyse duurt vrij lang (ongeveer 45 minuten op 1 cpu)
2. Analyse repeterend uitvoeren op zelfde codebase:
  ```mvn -DwithHistory org.pitest:pitest-maven:mutationCoverage ```
3. Analyse uitvoeren op toegevoegde en aangepaste files. (hiervoor moet nog e.e.a. ingericht worden voordat dit gaat werken)

Belangrijk:
1. Het kan zijn dat je meldingen krijgt als: ```"Warning : Minion exited abnormally due to TIMED_OUT"```
  Dit is vrij normaal en komt doordat PIT dan een mutatie gemaakt heeft die resulteert in een infinite loop.

# Markdown
Handigheidjes voor gebruik Markdown: <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>

